import { registerAs } from "@nestjs/config";

/**
 * En: Set database connection settings
 * Es: Establecer configuraciones de la conexión a la base de datos
 */
export const databaseConfig = registerAs("database", (): DatabaseConfig => ({
  drivers: {
    mysql: {
      type: "mysql",
      host: process.env.MYSQL_HOST || "localhost",
      port: parseInt(process.env.MYSQL_PORT, 10) || 3306,
      username: process.env.MYSQL_USERNAME || "default",
      password: process.env.MYSQL_PASSWORD || "secret",
      database: process.env.MYSQL_DATABASE || "",
      synchronize: process.env.APP_IS_DEVELOPMENT === "true" ? true : false
    },
    postgres: {
      type: "postgres",
      host: process.env.POSTGRES_HOST || "localhost",
      port: parseInt(process.env.POSTGRES_PORT, 10) || 5432,
      username: process.env.POSTGRES_USERNAME || "default",
      password: process.env.POSTGRES_PASSWORD || "secret",
      database: process.env.POSTGRES_DATABASE || "",
      synchronize: process.env.APP_IS_DEVELOPMENT === "true" ? true : false
    }
  }
}));

export interface DatabaseConfig {
  drivers: DriversDatabase
}

interface DriversDatabase {
  mysql: GenericDatabase
  postgres: GenericDatabase
}

interface GenericDatabase {
  type: "mysql" | "postgres"
  host: string
  port: number
  username: string
  password: string
  database: string
  synchronize: boolean
} 
