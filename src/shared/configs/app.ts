import { registerAs } from "@nestjs/config";

/**
 * En: Set application settings
 * Es: Establecer configuraciones de la aplicación
 */
export const appConfig = registerAs("app", (): AppConfig => ({
  name: process.env.APP_NAME,
  port: parseInt(process.env.APP_PORT, 10) || 3000
}));

export interface AppConfig {
  name: string
  port: number
}
