import { Module         } from "@nestjs/common";
import { ConfigModule   } from "@nestjs/config";
import { appConfig      } from "./app";
import { databaseConfig } from "./database";

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [
        appConfig, databaseConfig
      ]
    })
  ]
})
export class ConfigsModule {}
