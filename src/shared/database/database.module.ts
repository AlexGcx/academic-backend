import { Module             } from "@nestjs/common";
import { 
  ConfigModule,
  ConfigService             } from "@nestjs/config";
import { TypeOrmModule      } from "@nestjs/typeorm";
import { 
  PostgresConnectionService } from "./connections/postgres/postgres.service";

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      inject:   [ConfigService],
      imports:  [ConfigModule],
      useClass: PostgresConnectionService
    })
  ]
})
export class DatabaseModule {}
