import { Injectable                } from "@nestjs/common";
import { ConfigService             } from "@nestjs/config";
import { TypeOrmModuleOptions,
         TypeOrmOptionsFactory     } from "@nestjs/typeorm";
import { DatabaseConfig            } from "src/shared/configs/database";
import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";

@Injectable()
export class PostgresConnectionService implements TypeOrmOptionsFactory {
  /**
   * En: Postgres connection class constructor
   * Es: Constructor clase conexión postgres
   * @param config
   */
  constructor(private config: ConfigService) {}
  
  /**
   * En: Set configurations for postgres connection
   * Es: Establecer configuraciones para la conexión postgres
   * @return {*}
   */
  createTypeOrmOptions(): TypeOrmModuleOptions {
    const database = this.config.get<DatabaseConfig>("database");
    return {
      type: database.drivers.postgres.type,
      host: database.drivers.postgres.host,
      port: database.drivers.postgres.port,
      username: database.drivers.postgres.username,
      password: database.drivers.postgres.password,
      database: database.drivers.postgres.database,
      synchronize: true,
      autoLoadEntities: true
    } as PostgresConnectionOptions
  }
}
