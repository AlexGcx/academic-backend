import { ConfigModule               } from "@nestjs/config";
import { Test,
         TestingModule              } from "@nestjs/testing";
import { PostgresConnectionService  } from "./postgres.service";

describe("PostgresConnectionService", () => {
  let service: PostgresConnectionService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports:   [ConfigModule],
      providers: [PostgresConnectionService],
    }).compile();

    service = module.get<PostgresConnectionService>(PostgresConnectionService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });
});
